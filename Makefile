CLEF_CONFIG_PATH := ./config/clef
CRONOS_CHAIN_ID := localtest_1338-1
CRONOS_CONFIG_PATH := ./config/cronosd/.cronos
CRONOS_GENESIS_ACCOUNT := Default
KEYSTORE_PATH := ./keystore/geth

create-accounts:
	clef newaccount --keystore "$(KEYSTORE_PATH)"

start-node:
	sh ./scripts/init.sh

start-clef:
	clef --keystore "$(KEYSTORE_PATH)" \
		--configdir "$(CLEF_CONFIG_PATH)" \
		--chainid 1

start-geth:
	geth \
		--datadir ./data/geth \
		--signer ./config/clef/clef.ipc \
		--syncmode snap \
		--http.port 8757 \
		--http
