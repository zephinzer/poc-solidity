# POC Solidity

It's May 7th 2022 and a bot just beat me to redeeming bonds for a tomb fork I'm participating in. Well, that sucks. So this is my adventure getting started in the world of smart contracts using Cronos, an EVM-compatible chain.

# Pre-Requisites

1. Install **Golang** ([Link to instructions](https://go.dev/doc/install))
2. Get the **Cronos Testnet binary** ([Link to instructions](https://cronos.org/docs/getting-started/cronos-testnet.html#step-1-get-the-cronos-testnet-binary))
3. Install **the Solidity compiler `solc`** ([Link to instructions](https://docs.soliditylang.org/en/latest/installing-solidity.html))
4. Install **`geth` & tools** ([Link to instructions](https://geth.ethereum.org/downloads/))

# Overview of Tools

| Tool      | Description                                                     |
| --------- | --------------------------------------------------------------- |
| `abigen`  | The ABI generator for smart contracts                           |
| `clef`    | Account management tool for EVM-compatible chains               |
| `cronosd` | The Cronos binary                                               |
| `geth`    | An Ethereum client written in Go                                |
| `go`      | The programming language we will use to interact with the chain |
| `solc`    | The compiler for Solidity smart contracts                       |

## How it all links up

1. We will be starting a local devnet chain using the `cronosd` binary
3. We run an EVM client using `geth` that connects to the devnet chain
2. We will generate accounts using `clef`
3. We will compile Solidity smart contraccts using `solc`
4. We will generate the ABI using `abigen`
5. We will interact with `geth` using a custom binary written in `go`

# Tutorial

All commands are ran via `make <command>`, you can open the [`./Makefile`](./Makefile) to see the exact commands if you're curious

## Start Cronos Testnet node

Run the following to start a Cronos node:

```sh
make start-node;
```

> This command runs the generic `init.sh` which I've modified to use a local path, `./config/cronosd` to be specific

The process should be running in the foreground, so continue in another terminal.

## Generate accounts

Run the following to populate the keystore directory with an account:

```sh
make create-accounts;

# enter in a password that is >10 characters in length
```

## Start Clef

Run the following to start Clef:

```sh
make start-clef;
```

## Start Geth

Run the following to start Geth:

```sh
make start-geth;
```




# References

- [Devnet: Running Latest Development Node](https://cronos.org/docs/getting-started/local-devnet.html#overview)
- [Original `./scripts/init.sh` file](https://raw.githubusercontent.com/crypto-org-chain/cronos-docs/master/docs/getting-started/assets/init_cronos_chain/init.sh)
