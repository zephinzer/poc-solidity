#!/bin/bash

KEY="Default"
CHAINID="localtest_1338-1"
MONIKER="localtest"
KEYRING="file"
KEYALGO="eth_secp256k1"
LOGLEVEL="info"
# to trace evm
TRACE="--trace"
# TRACE=""
WORKDIR="./config/cronosd"

# validate dependencies are installed
command -v jq > /dev/null 2>&1 || { echo >&2 "jq not installed. More info: https://stedolan.github.io/jq/download/"; exit 1; }

# remove existing daemon and client
rm -rf ${WORKDIR}/.cronos*

rm ${WORKDIR}/keyring-file/*
rm ${WORKDIR}/config/genesis.json
rm ${WORKDIR}/config/gentx/*

make install
cronosd unsafe-reset-all --home ${WORKDIR}
cronosd config keyring-backend $KEYRING --home ${WORKDIR}
cronosd config chain-id $CHAINID --home ${WORKDIR}

# if $KEY exists it should be deleted
cronosd keys add $KEY --keyring-backend $KEYRING --algo $KEYALGO --home ${WORKDIR}

# Set moniker and chain-id for Ethermint (Moniker can be anything, chain-id must be an integer)
cronosd init $MONIKER --chain-id $CHAINID --home ${WORKDIR}

# Change parameter token denominations to stake
cat ${WORKDIR}/config/genesis.json | jq '.app_state["staking"]["params"]["bond_denom"]="stake"' > ${WORKDIR}/config/tmp_genesis.json && mv ${WORKDIR}/config/tmp_genesis.json ${WORKDIR}/config/genesis.json
cat ${WORKDIR}/config/genesis.json | jq '.app_state["crisis"]["constant_fee"]["denom"]="stake"' > ${WORKDIR}/config/tmp_genesis.json && mv ${WORKDIR}/config/tmp_genesis.json ${WORKDIR}/config/genesis.json
cat ${WORKDIR}/config/genesis.json | jq '.app_state["gov"]["deposit_params"]["min_deposit"][0]["denom"]="stake"' > ${WORKDIR}/config/tmp_genesis.json && mv ${WORKDIR}/config/tmp_genesis.json ${WORKDIR}/config/genesis.json
cat ${WORKDIR}/config/genesis.json | jq '.app_state["mint"]["params"]["mint_denom"]="stake"' > ${WORKDIR}/config/tmp_genesis.json && mv ${WORKDIR}/config/tmp_genesis.json ${WORKDIR}/config/genesis.json
cat ${WORKDIR}/config/genesis.json | jq '.app_state["evm"]["params"]["evm_denom"]="stake"' > ${WORKDIR}/config/tmp_genesis.json && mv ${WORKDIR}/config/tmp_genesis.json ${WORKDIR}/config/genesis.json

# increase block time (?)
cat ${WORKDIR}/config/genesis.json | jq '.consensus_params["block"]["time_iota_ms"]="1000"' > ${WORKDIR}/config/tmp_genesis.json && mv ${WORKDIR}/config/tmp_genesis.json ${WORKDIR}/config/genesis.json

# Set gas limit in genesis
cat ${WORKDIR}/config/genesis.json | jq '.consensus_params["block"]["max_gas"]="10000000"' > ${WORKDIR}/config/tmp_genesis.json && mv ${WORKDIR}/config/tmp_genesis.json ${WORKDIR}/config/genesis.json

# disable produce empty block
if [[ "$OSTYPE" == "darwin"* ]]; then
    sed -i '' 's/create_empty_blocks = true/create_empty_blocks = false/g' ${WORKDIR}/config/config.toml
  else
    sed -i 's/create_empty_blocks = true/create_empty_blocks = false/g' ${WORKDIR}/config/config.toml
fi

if [[ $1 == "pending" ]]; then
  if [[ "$OSTYPE" == "darwin"* ]]; then
      sed -i '' 's/create_empty_blocks_interval = "0s"/create_empty_blocks_interval = "30s"/g' ${WORKDIR}/config/config.toml
      sed -i '' 's/timeout_propose = "3s"/timeout_propose = "30s"/g' ${WORKDIR}/config/config.toml
      sed -i '' 's/timeout_propose_delta = "500ms"/timeout_propose_delta = "5s"/g' ${WORKDIR}/config/config.toml
      sed -i '' 's/timeout_prevote = "1s"/timeout_prevote = "10s"/g' ${WORKDIR}/config/config.toml
      sed -i '' 's/timeout_prevote_delta = "500ms"/timeout_prevote_delta = "5s"/g' ${WORKDIR}/config/config.toml
      sed -i '' 's/timeout_precommit = "1s"/timeout_precommit = "10s"/g' ${WORKDIR}/config/config.toml
      sed -i '' 's/timeout_precommit_delta = "500ms"/timeout_precommit_delta = "5s"/g' ${WORKDIR}/config/config.toml
      sed -i '' 's/timeout_commit = "5s"/timeout_commit = "150s"/g' ${WORKDIR}/config/config.toml
      sed -i '' 's/timeout_broadcast_tx_commit = "10s"/timeout_broadcast_tx_commit = "150s"/g' ${WORKDIR}/config/config.toml
  else
      sed -i 's/create_empty_blocks_interval = "0s"/create_empty_blocks_interval = "30s"/g' ${WORKDIR}/config/config.toml
      sed -i 's/timeout_propose = "3s"/timeout_propose = "30s"/g' ${WORKDIR}/config/config.toml
      sed -i 's/timeout_propose_delta = "500ms"/timeout_propose_delta = "5s"/g' ${WORKDIR}/config/config.toml
      sed -i 's/timeout_prevote = "1s"/timeout_prevote = "10s"/g' ${WORKDIR}/config/config.toml
      sed -i 's/timeout_prevote_delta = "500ms"/timeout_prevote_delta = "5s"/g' ${WORKDIR}/config/config.toml
      sed -i 's/timeout_precommit = "1s"/timeout_precommit = "10s"/g' ${WORKDIR}/config/config.toml
      sed -i 's/timeout_precommit_delta = "500ms"/timeout_precommit_delta = "5s"/g' ${WORKDIR}/config/config.toml
      sed -i 's/timeout_commit = "5s"/timeout_commit = "150s"/g' ${WORKDIR}/config/config.toml
      sed -i 's/timeout_broadcast_tx_commit = "10s"/timeout_broadcast_tx_commit = "150s"/g' ${WORKDIR}/config/config.toml
  fi
fi

# Allocate genesis accounts (cosmos formatted addresses)
cronosd add-genesis-account $KEY 100000000000000000000000000stake --keyring-backend $KEYRING --home ${WORKDIR}

# Sign genesis transaction
cronosd gentx $KEY 1000000000000000000000stake --keyring-backend $KEYRING --chain-id $CHAINID --home ${WORKDIR}

# Collect genesis tx
cronosd collect-gentxs --home ${WORKDIR}

# Run this to ensure everything worked and that the genesis file is setup correctly
cronosd validate-genesis --home ${WORKDIR}

if [[ $1 == "pending" ]]; then
  echo "pending mode is on, please wait for the first block committed."
fi

# Start the node (remove the --pruning=nothing flag if historical queries are not needed)
cronosd start --pruning=nothing --evm.tracer=json $TRACE --log_level $LOGLEVEL --minimum-gas-prices=0stake --json-rpc.api eth,txpool,personal,net,debug,web3,miner --home ${WORKDIR}
